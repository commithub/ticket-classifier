import Grid from "@mui/material/Grid";
import Stack from "@mui/material/Stack";
import muiSpacingDefaults from "modules/constants/muiSpacingDefaults/muiSpacingDefaults";

function ActionButtonGroup({ children, justifyContent }) {
  return (
    <Grid item xs data-testid="actionButtonGroup">
      <Stack
        direction={{ xs: "column", sm: "row" }}
        justifyContent={justifyContent}
        spacing={muiSpacingDefaults.stack}
      >
        {children}
      </Stack>
    </Grid>
  );
}

export default ActionButtonGroup;
