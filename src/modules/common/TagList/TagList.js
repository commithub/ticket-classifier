import Chip from "@mui/material/Chip";
import Stack from "@mui/material/Stack";
import muiSpacingDefaults from "modules/constants/muiSpacingDefaults/muiSpacingDefaults";

function TagList(props) {
  return (
    <Stack
      direction={{ xs: "column", sm: "row" }}
      flexWrap="wrap"
      spacing={muiSpacingDefaults.stack}
      sx={muiSpacingDefaults.marginY}
      data-testid="tagList"
    >
      {props.tags.map((tag) => (
        <Chip
          key={tag.id}
          label={tag.label}
          color={tag.color}
          data-testid="tagListChip"
        />
      ))}
    </Stack>
  );
}

export default TagList;
