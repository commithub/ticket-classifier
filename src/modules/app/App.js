import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import TicketList from "modules/ticketDashboard/TicketList/TicketList";
import muiSpacingDefaults from "modules/constants/muiSpacingDefaults/muiSpacingDefaults";

function App() {
  return (
    <Container>
      <Typography
        sx={muiSpacingDefaults.marginY}
        color="textPrimary"
        variant="h1"
        data-testid="appTitle"
      >
        Ticket Classifier
      </Typography>
      <TicketList />
    </Container>
  );
}

export default App;
