import { render } from "@testing-library/react";
import App from "./App";

describe("App", () => {
  test("renders Ticket Classifier header", () => {
    const { getByTestId } = render(<App />);
    const headerElement = getByTestId("appTitle");
    expect(headerElement).toBeInTheDocument();
    expect(headerElement.textContent).toEqual("Ticket Classifier");
  });

  test("renders a container for the ticketList", () => {
    const { getByTestId } = render(<App />);
    const ticketList = getByTestId("ticketList");
    expect(ticketList).toBeInTheDocument();
  });

  test("renders a list of tickets", () => {
    const { queryAllByTestId } = render(<App />);
    const tickets = queryAllByTestId("ticketCard");
    expect(tickets.length).toBeGreaterThan(0);
  });
});
