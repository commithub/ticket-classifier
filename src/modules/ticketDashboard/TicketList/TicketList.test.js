import { render } from "@testing-library/react";
import TicketList from "./TicketList";

const renderTicketList = () => render(<TicketList />);

describe("TicketList", () => {
  test("renders component", () => {
    const { getByTestId } = renderTicketList();
    const ticketList = getByTestId("ticketList");
    expect(ticketList).toBeInTheDocument();
  });

  test("renders a list of tickets", () => {
    const { queryAllByTestId } = renderTicketList();
    const tickets = queryAllByTestId("ticketCard");
    expect(tickets.length).toBeGreaterThan(0);
  });
});
