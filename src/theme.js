import { createTheme } from "@mui/material/styles";

export default createTheme({
  palette: {
    type: "light",
    primary: {
      main: "#a43fcf",
      contrastText: "#FCFCFC",
    },
    secondary: {
      main: "#edf1f7",
      contrastText: "#1C1C1C",
    },
    error: {
      main: "#e02626",
      contrastText: "#FCFCFC",
    },
    warning: {
      main: "#ffc700",
      contrastText: "#1C1C1C",
    },
    info: {
      main: "#0088ff",
      contrastText: "#1C1C1C",
    },
    success: {
      main: "#97CE0C",
      contrastText: "#1C1C1C",
    },
    background: {
      default: "#FCFCFC",
    },
  },
  typography: {
    fontFamily: "Montserrat",
    fontSize: 16,
    h1: {
      fontSize: 36,
      fontWeight: 700,
    },
    h2: {
      fontSize: 32,
      fontWeight: 700,
    },
    h3: {
      fontSize: 30,
      fontWeight: 700,
    },
    h4: {
      fontSize: 26,
      fontWeight: 700,
    },
    h5: {
      fontSize: 22,
      fontWeight: 700,
    },
    h6: {
      fontSize: 18,
      fontWeight: 700,
    },
    fontWeightLight: 400,
  },
});
