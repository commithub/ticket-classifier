import React from "react";
import ReactDOM from "react-dom";

import { ThemeProvider } from "@mui/material/styles";
import theme from "theme";
import App from "modules/app/App";

import "sass/index.scss";
import "@fontsource/montserrat/300.css";
import "@fontsource/montserrat/400.css";
import "@fontsource/montserrat/500.css";
import "@fontsource/montserrat/700.css";
import "@mui/icons-material";

ReactDOM.render(
  <React.StrictMode>
    <ThemeProvider theme={theme}>
      <App />
    </ThemeProvider>
  </React.StrictMode>,
  document.getElementById("root")
);
